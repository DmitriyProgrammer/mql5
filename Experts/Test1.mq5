//+------------------------------------------------------------------+
//|                                                        Test1.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <OClass.mqh>

Order* ord;

int OnInit()
{
    ord = new Order();
    return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason)
{
    delete ord;
   
}

void OnTick()
{
   ord.SetLots(0.01);
   ord.SetSL(100);
   ord.SetTP(100);
   ord.Buy();
   
}

void OnTrade()
{
   
}
