//+------------------------------------------------------------------+
//|                                                        Test2.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include <Trade\Trade.mqh>

input double Lot        = 0.1;
input int    TakeProfit = 10;
input int    StopLoss   = 65;

int TP;
int SL;

CTrade trader;

bool Invertor = true;

int OnInit()
{

   TP = TakeProfit;
   SL = StopLoss;
   
   if(_Digits == 3 || _Digits == 5) {
      TP = TP * 10;
      SL = SL * 10;
   }
   
   return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason)
{
   
}

void OnTick()
{
   double points;
   
   if(!PositionSelect(_Symbol)) {
      if(Invertor == true) {
         trader.Buy(Lot);
      }else {
         trader.Sell(Lot);
      }
   } else {
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY) {
         points = (SymbolInfoDouble(_Symbol,SYMBOL_BID) - PositionGetDouble(POSITION_PRICE_OPEN)) / _Point;
         if(points >= TP) {
            trader.PositionClose(_Symbol);
            Invertor = true;
         }
         
         if(points <= -SL) {
            trader.PositionClose(_Symbol);
            Invertor = false;
         }
      }
      
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL) {
         points = (PositionGetDouble(POSITION_PRICE_OPEN) - SymbolInfoDouble(_Symbol, SYMBOL_ASK)) / _Point;
         if(points >= TP) {
            trader.PositionClose(_Symbol);
            Invertor = false;
         }
         
         if(points <= -SL) {
            trader.PositionClose(_Symbol);
            Invertor = true;
         }
         
      }
      
   }
   
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
   
  }
//+------------------------------------------------------------------+
