class Order {

   private:
   MqlTradeRequest             o_request;
   MqlTradeResult              o_result;
   
   int                         OrderSL;
   int                         OrderTP;
   
   double                      ord_SL;
   double                      ord_TP;
   
   public:
   Order(){
      ZeroMemory(o_request);
      
      o_request.action       = TRADE_ACTION_DEAL;
      o_request.magic        = 0;
      o_request.symbol       = Symbol();
      o_request.volume       = 0.1;
      o_request.price        = 0;
      o_request.sl           = 0;
      o_request.deviation    = 5;
      o_request.type         = 0;
      o_request.type_filling = ORDER_FILLING_FOK;
      o_request.type_time    = ORDER_TIME_GTC;
      o_request.expiration   = 0;
      
      OrderSL                = 0;
      OrderTP                = 0;
      
      ord_SL                 = 0;
      ord_TP                 = 0;
      
   }
   
   double GetSL(bool IsBuyOrder) {
      if(OrderSL > 0) {
         if(IsBuyOrder) {
            return (SymbolInfoDouble(o_request.symbol, SYMBOL_ASK) - OrderSL * _Point);
         } else {
            return (SymbolInfoDouble(o_request.symbol, SYMBOL_BID) + OrderSL * _Point);
         }
      } else {
         return (ord_SL);
      }
   }
   
   double GetTP(bool IsBuyOrder) {
      if(OrderTP > 0) {
         if(IsBuyOrder) {
            return (SymbolInfoDouble(o_request.symbol, SYMBOL_ASK) + OrderTP * _Point);
         } else {
            return (SymbolInfoDouble(o_request.symbol, SYMBOL_BID) - OrderTP * _Point);
         }
      } else {
         return (ord_TP);
      }
   }
   
   bool Execute(){
      bool err = OrderSend(o_request, o_result);
      if(!err) {
         Print("Ошибка исполнения ордера");
      }
      
      return (err);
   }
   
   bool Buy() {
      o_request.type         = ORDER_TYPE_BUY;
      o_request.price        = SymbolInfoDouble(o_request.symbol, SYMBOL_ASK);
      o_request.sl           = GetSL(true);
      o_request.tp           = GetTP(true);
      return (Execute());
   }
   
   bool Sell() {
      o_request.type         = ORDER_TYPE_SELL;
      o_request.price        = SymbolInfoDouble(o_request.symbol, SYMBOL_BID);
      o_request.sl           = GetSL(false);
      o_request.tp           = GetTP(false);
      return (Execute());
   }
   
   bool Close() {
      bool ret;
      if(PositionSelect(o_request.symbol)){
         if(ENUM_POSITION_TYPE(PositionGetInteger(POSITION_TYPE)) == POSITION_TYPE_BUY) {
            o_request.type   = ORDER_TYPE_SELL;
            o_request.price  = SymbolInfoDouble(o_request.symbol, SYMBOL_BID);
         }else {
            o_request.type   = ORDER_TYPE_BUY;
            o_request.price  = SymbolInfoDouble(o_request.symbol, SYMBOL_ASK);
         }
         
         double vol          = o_request.volume;
         o_request.sl        = 0;
         o_request.tp        = 0;
         o_request.volume    = PositionGetDouble(POSITION_VOLUME);
         ret                 = Execute();
         o_request.volume    = vol;
         
      }else {
         Print("Не удалось выбрать позицию CLOSE");
         ret = false;
      }
      
      return (ret);
   }
   
   void SetMagic (int magic) {
      o_request.magic        = magic;
   }
   
   void SetComment(string comment) {
      o_request.comment      = comment;
   }
   
   void SetLots (double lots) {
      o_request.volume       = lots;
   }
   
   void SetSL(int sl) {
      ord_SL                 = 0;
      OrderSL                = sl;
   }
   
   void SetTP(int tp) {
      ord_TP                 = 0;
      OrderTP                = tp;
   }

};